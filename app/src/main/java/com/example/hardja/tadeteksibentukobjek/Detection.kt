package com.example.hardja.tadeteksibentukobjek

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import kotlinx.android.synthetic.main.detection.*
import org.opencv.android.*
import org.opencv.core.*
import org.opencv.core.Scalar
import org.opencv.features2d.FeatureDetector
import org.opencv.imgproc.Imgproc


class Detection : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2 {
    private var mRgba: Mat? = null
    private var intermediateMat: Mat = Mat()
    private val bw = Mat()
    private val hsv = Mat()
    private val lowerRedRange = Mat()
    private val upperRedRange = Mat()
    private val downscaled = Mat()
    private val upscaled = Mat()
    private var contourImage = Mat()
    private val hierarchyOutputVector = Mat()
    private var approxCurve = MatOfPoint2f()
    private var cameraBridgeViewBase: CameraBridgeViewBase? = null
    private val loaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.i(TAG, "OpenCV loaded successfully")
                    kameracv.enableView()
                }
                else -> super.onManagerConnected(status)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.detection)
        cameraBridgeViewBase = findViewById<CameraBridgeViewBase>(R.id.kameracv)
        cameraBridgeViewBase!!.setCvCameraViewListener(this)
        cameraBridgeViewBase!!.setMaxFrameSize(1280, 720)
    }


    override fun onResume() {
        super.onResume()
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, loaderCallback)
    }

    override fun onPause() {
        super.onPause()
        if (kameracv != null)
            kameracv!!.disableView()
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        mRgba = Mat(height, width, CvType.CV_8UC4)
        intermediateMat = Mat(height, width, CvType.CV_8UC4)
    }

    override fun onCameraViewStopped() {
        mRgba?.release()
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat {
        mRgba = inputFrame.rgba()
//        detectCircle(inputFrame.gray())
//        detectObject(inputFrame.gray())
        val gray = if (DETECT_RED_OBJECTS_ONLY) inputFrame.rgba() else inputFrame.gray()
        detectShape(gray)
        return mRgba!!
    }

    private fun detectShape(gray: Mat) {
        Imgproc.pyrDown(gray, downscaled, Size((gray.cols() / 2).toDouble(), (gray.rows() / 2).toDouble()))
        Imgproc.pyrUp(downscaled, upscaled, gray.size())
        if (DETECT_RED_OBJECTS_ONLY) {
            Imgproc.cvtColor(upscaled, hsv, Imgproc.COLOR_RGB2HSV)
            Core.inRange(hsv, HSV_LOW_RED1, HSV_LOW_RED2, lowerRedRange)
            Core.inRange(hsv, HSV_HIGH_RED1, HSV_HIGH_RED2, upperRedRange)
            Core.addWeighted(lowerRedRange, 1.0, upperRedRange, 1.0, 0.0, bw)
            Imgproc.Canny(bw, bw, 0.0, 255.0)
        } else {
            Imgproc.Canny(upscaled, bw, 0.0, 255.0)
        }
        Imgproc.dilate(bw, bw, Mat(), Point(-1.0, 1.0), 1)
        val contours = arrayListOf<MatOfPoint>()
        contourImage = bw.clone()
        Imgproc.findContours(
                contourImage,
                contours,
                hierarchyOutputVector,
                Imgproc.RETR_EXTERNAL,
                Imgproc.CHAIN_APPROX_SIMPLE
        )
        for (cnt in contours) {
            val curve = MatOfPoint2f()
            curve.fromList(cnt.toList())
            Imgproc.approxPolyDP(
                    curve,
                    approxCurve,
                    0.02 * Imgproc.arcLength(curve, true),
                    true
            )
            val numOfVertices = approxCurve.total().toInt()
            Log.d("numOfVertices", numOfVertices.toString())
            val contourArea = Imgproc.contourArea(curve)
            if (Math.abs(contourArea) < 100)
                continue
            if (numOfVertices == 3) {
                setLabel(mRgba!!, "TRIANGLE", cnt)
            } else if (numOfVertices in 4..6) {
                val cos = mutableListOf<Double>()
                for (j in 2..numOfVertices) {
                    val appArr = approxCurve.toArray()
                    cos.add(angel(
                            appArr[j % numOfVertices],
                            appArr[j - 2],
                            appArr[j - 1])
                    )
                }
                cos.sort()
                val mincos = cos[0]
                val maxcos = cos.last()

                if (numOfVertices == 4 /*&& mincos >= -0.1 && maxcos <= 0.3*/) {
                    setLabel(mRgba!!, "SQUARE", cnt)
                } else if (numOfVertices == 5 /*&& mincos >= -0.34 && maxcos <= -0.27*/) {
                    setLabel(mRgba!!, "PENTAGON", cnt)
                } else if (numOfVertices == 6 /*&& mincos >= -0.55 && maxcos <= -0.45*/) {
                    setLabel(mRgba!!, "HEXAGON", cnt)
                }
            } else {
                val r = Imgproc.boundingRect(cnt)
                val radius = r.width / 2
                if (Math.abs(1 - (r.width / r.height)) <= 0.2 && Math.abs(1 - (contourArea / (Math.PI * radius * radius))) <= 0.2) {
                    setLabel(mRgba!!, "CIRCLE", cnt)
                }
            }
        }
    }

    private fun detectObject(input: Mat) {
        CONTOUR_COLOR = Scalar(255.0)
        val matKeyPoint = MatOfKeyPoint()
        var lispoint: List<KeyPoint> = listOf()
        var keyPoint: KeyPoint
        val mask = Mat.zeros(input.size(), CvType.CV_8UC1)
        var rectanx1: Int
        var rectany1: Int
        var rectanx2: Int
        var rectany2: Int
        val imgSize = input.height().times(input.width())
        val zeros = Scalar(0.0, 0.0, 0.0)
        val contour2 = arrayListOf<MatOfPoint>()
        val kernel = Mat(1, 50, CvType.CV_8UC1, Scalar.all(255.0))
        val morbyte = Mat()
        val hierarchy = Mat()
        var rectan3: Rect? = null
        val detector = FeatureDetector.create(FeatureDetector.FAST)
        detector.detect(input, matKeyPoint)
        lispoint = matKeyPoint.toList()
        for (i in lispoint.indices) {
            keyPoint = lispoint[i]
            rectanx1 = (keyPoint.pt.x - 0.5 * keyPoint.size).toInt()
            rectany1 = (keyPoint.pt.y - 0.5 * keyPoint.size).toInt()
            rectanx2 = keyPoint.size.toInt()
            rectany2 = keyPoint.size.toInt()
            if (rectanx1 < 0) rectanx1 = 1
            if (rectany1 < 0) rectany1 = 1
            if ((rectanx1 + rectanx2) > input.width())
                rectanx2 = input.width() - rectanx1
            if ((rectany1 + rectany2) > input.height())
                rectany2 = input.height() - rectany1
            val rectant = Rect(rectanx1, rectany1, rectanx2, rectany2)
            try {
                val roi = Mat(mask, rectant)
                roi.setTo(CONTOUR_COLOR)
            } catch (ex: Exception) {
                Log.d("ROI", "MAT ROI ERROR")
                ex.printStackTrace()
            }
        }
        Imgproc.morphologyEx(mask, morbyte, Imgproc.MORPH_DILATE, kernel)
        Imgproc.findContours(morbyte, contour2, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE)
        for (co in contour2) {
            rectan3 = Imgproc.boundingRect(co)
            if (rectan3.area() < imgSize * AREA_THRESHOLD)
                continue
            var bmp: Bitmap? = null
            try {
                val croppedPart = Mat(input, rectan3)
                bmp = Bitmap.createBitmap(croppedPart.width(), croppedPart.height(), Bitmap.Config.ARGB_8888)
                Utils.matToBitmap(croppedPart, bmp)
                Log.d("CROPPING", "SUCCESS")
            } catch (ex: Exception) {
                Log.e("CROPPING", "FAIL")
                ex.printStackTrace()
            }
            if (rectan3.area() > 0.5 * imgSize || rectan3.area() < 100 || (rectan3.width / rectan3.height) < 2) {
                val roi = Mat(morbyte, rectan3)
                roi.setTo(zeros)
            } else {
                Imgproc.rectangle(mRgba, rectan3.br(), rectan3.tl(), CONTOUR_COLOR)
            }

            if (bmp != null)
                Log.d("BITMAP", "BITMAP FOUND!")
        }
    }

    private fun detectCircle(input: Mat) {
        val circles = Mat()
        Imgproc.blur(input, input, Size(7.0, 7.0), Point(2.0, 2.0))
        Imgproc.HoughCircles(input, circles, Imgproc.CV_HOUGH_GRADIENT, 2.0, 100.0, 100.0, 90.0, 0, 1000)
        Log.i(TAG, "size: " + circles.cols() + ", " + circles.rows().toString())
        if (circles.cols() > 0) {
            for (x in 0 until Math.min(circles.cols(), 5)) {
                val circleVec = circles.get(0, x) ?: break
                val center = Point(circleVec[0].toInt().toDouble(), circleVec[1].toInt().toDouble())
                val radius = circleVec[2].toInt()
                Imgproc.circle(input, center, 3, Scalar(255.0, 255.0, 255.0), 5)
                Imgproc.circle(input, center, radius, Scalar(255.0, 255.0, 255.0), 2)
            }
        }
        circles.release()
        input.release()
    }

    companion object {
        val TAG = "src"
        private var CONTOUR_COLOR: Scalar? = null
        private val AREA_THRESHOLD = 0.025
        private val HSV_LOW_RED1 = Scalar(0.0, 100.0, 100.0)
        private val HSV_LOW_RED2 = Scalar(10.0, 255.0, 255.0)
        private val HSV_HIGH_RED1 = Scalar(160.0, 100.0, 100.0)
        private val HSV_HIGH_RED2 = Scalar(179.0, 255.0, 255.0)
        private val RGB_RED = Scalar(255.0, 0.0, 0.0)
        private val FRAME_SIZE_WIDTH = 640
        private val FRAME_SIZE_HEIGHT = 480
        private val FIXED_FRAME_SIZE = true
        private val DETECT_RED_OBJECTS_ONLY = false
        private val LOG_MEM_USAGE = true

        init {
            if (!OpenCVLoader.initDebug()) {
                Log.d(TAG, "OpenCV failed to load!")
            }
        }

        fun angel(pt1: Point, pt2: Point, pt0: Point): Double {
            val dx1 = pt1.x - pt0.x
            val dy1 = pt1.y - pt0.y
            val dx2 = pt2.x - pt0.x
            val dy2 = pt2.y - pt0.y
            return (dx1 * dx2 + dy1 + dy2) / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10)
        }

        fun setLabel(im: Mat, label: String, contour: MatOfPoint) {
            val fontface = Core.FONT_HERSHEY_COMPLEX
            val scale = 2.0
            val thickness = 3
            val baseline = IntArray(1)
            val text = Imgproc.getTextSize(label, fontface, scale, thickness, baseline)
            val r = Imgproc.boundingRect(contour)
            val pt = Point(r.x + ((r.width - text.width) / 2),
                    r.y + ((r.height + text.height) / 2))
            Imgproc.putText(im, label, pt, fontface, scale, RGB_RED, thickness)
//            rotateText(im, 45.0, im)
        }

        fun rotateText(im: Mat, deg: Double, dest: Mat) {
            val len = Math.max(im.cols(), im.rows())
            val pt = Point((len / 2).toDouble(), (len / 2).toDouble())
            val r = Imgproc.getRotationMatrix2D(pt, deg, 1.0)
            Imgproc.warpAffine(im, dest, r, Size(len.toDouble(), len.toDouble()))
        }
    }
}