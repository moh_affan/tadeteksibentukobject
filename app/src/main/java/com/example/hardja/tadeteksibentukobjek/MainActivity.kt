package com.example.hardja.tadeteksibentukobjek

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ibdetection.setOnClickListener {
            val i = Intent(applicationContext, Detection::class.java)
            startActivity(i)
        }

        ibdictonary.setOnClickListener {
            val i = Intent(applicationContext, Dictonary::class.java)
            startActivity(i)
        }

        ibexit.setOnClickListener { exit() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onBackPressed() {
        exit()
    }

    private fun exit() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are You Sure Want To Exit?")
                .setCancelable(false)
                .setPositiveButton("Yes") { _, _ -> finish() }
                .setNegativeButton("No") { dialog, _ -> dialog.cancel() }.show()
    }

}