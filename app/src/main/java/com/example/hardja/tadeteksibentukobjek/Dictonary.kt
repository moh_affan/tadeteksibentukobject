package com.example.hardja.tadeteksibentukobjek

import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageButton
import android.widget.Toast

class Dictonary : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dictonary)

        val click1 = findViewById<View>(R.id.Segitiga) as ImageButton
        val click2 = findViewById<View>(R.id.Segiempat) as ImageButton
        val click3 = findViewById<View>(R.id.Segilima) as ImageButton
        val click4 = findViewById<View>(R.id.Segienam) as ImageButton
        val click5 = findViewById<View>(R.id.Segitujuh) as ImageButton
        val click6 = findViewById<View>(R.id.Segidelapan) as ImageButton
        val click7 = findViewById<View>(R.id.lonjong) as ImageButton
        val click8 = findViewById<View>(R.id.Bulat) as ImageButton
        val click9 = findViewById<View>(R.id.Wajik) as ImageButton
        val click10 = findViewById<View>(R.id.Trapesium) as ImageButton
        val click11 = findViewById<View>(R.id.Bintang) as ImageButton
        val click12 = findViewById<View>(R.id.Persegipanjang) as ImageButton

        val mp1 = MediaPlayer.create(applicationContext, R.raw.segitiga)
        val mp2 = MediaPlayer.create(applicationContext, R.raw.kotak)
        val mp3 = MediaPlayer.create(applicationContext, R.raw.segilima)
        val mp4 = MediaPlayer.create(applicationContext, R.raw.segienam)
        val mp5 = MediaPlayer.create(applicationContext, R.raw.segitujuh)
        val mp6 = MediaPlayer.create(applicationContext, R.raw.segidelapan)
        val mp7 = MediaPlayer.create(applicationContext, R.raw.lonjong)
        val mp8 = MediaPlayer.create(applicationContext, R.raw.bulat)
        val mp9 = MediaPlayer.create(applicationContext, R.raw.wajik)
        val mp10 = MediaPlayer.create(applicationContext, R.raw.trapesium)
        val mp11 = MediaPlayer.create(applicationContext, R.raw.bintang)
        val mp12 = MediaPlayer.create(applicationContext, R.raw.persegi)


        val elem = View.OnClickListener { v ->
            when (v.id) {
                R.id.Segitiga -> {
                    Toast.makeText(this@Dictonary, "TRIANGEL", Toast.LENGTH_SHORT).show()
                    mp1.start()
                }
                R.id.Segiempat -> {
                    Toast.makeText(this@Dictonary, "SQUARE", Toast.LENGTH_SHORT).show()
                    mp2.start()
                }
                R.id.Segilima -> {
                    Toast.makeText(this@Dictonary, "PENTAGON", Toast.LENGTH_SHORT).show()
                    mp3.start()
                }
                R.id.Segienam -> {
                    Toast.makeText(this@Dictonary, "HEXAGON", Toast.LENGTH_SHORT).show()
                    mp4.start()
                }
                R.id.Segitujuh -> {
                    Toast.makeText(this@Dictonary, "HEPTAGON", Toast.LENGTH_SHORT).show()
                    mp5.start()
                }
                R.id.Segidelapan -> {
                    Toast.makeText(this@Dictonary, "OCTAGON", Toast.LENGTH_SHORT).show()
                    mp6.start()
                }
                R.id.lonjong -> {
                    Toast.makeText(this@Dictonary, "OVAL", Toast.LENGTH_SHORT).show()
                    mp7.start()
                }
                R.id.Bulat -> {
                    Toast.makeText(this@Dictonary, "CIRCLE", Toast.LENGTH_SHORT).show()
                    mp8.start()
                }
                R.id.Wajik -> {
                    Toast.makeText(this@Dictonary, "DIAMOND", Toast.LENGTH_SHORT).show()
                    mp9.start()
                }
                R.id.Trapesium -> {
                    Toast.makeText(this@Dictonary, "TRAPEZOID", Toast.LENGTH_SHORT).show()
                    mp10.start()
                }
                R.id.Bintang -> {
                    Toast.makeText(this@Dictonary, "STAR", Toast.LENGTH_SHORT).show()
                    mp11.start()
                }
                R.id.Persegipanjang -> {
                    Toast.makeText(this@Dictonary, "RECTANGLE", Toast.LENGTH_SHORT).show()
                    mp12.start()
                }
            }
        }
        click1.setOnClickListener(elem)
        click2.setOnClickListener(elem)
        click3.setOnClickListener(elem)
        click4.setOnClickListener(elem)
        click5.setOnClickListener(elem)
        click6.setOnClickListener(elem)
        click7.setOnClickListener(elem)
        click8.setOnClickListener(elem)
        click9.setOnClickListener(elem)
        click10.setOnClickListener(elem)
        click11.setOnClickListener(elem)
        click12.setOnClickListener(elem)
    }
}